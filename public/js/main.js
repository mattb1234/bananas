$(document).ready(function (){
    var config = [];
    var packs = [];
    var value;

    function error(_message, _container, _timeout)
    {
        if (typeof _timeout != 'undefined')
            _timeout = true;
        var alert = $('<div></div>').addClass('alert alert-danger').html('<i class="fa fa-times"></i> ' + _message);

        if ($(_container).find('.alert').length == 0)
            $(_container).prepend(alert);

        if (!_timeout)
            setTimeout(function (){
                alert.fadeOut(600);
            }, 800);
    }

    function refreshConfig(){
        $.ajax({
            url: '/api/config?sort=size&order=desc',
            type: 'GET',
            error: function() {
                error("Unable to load current pack configuration.", "#home .container-fluid", false);
            },
            success: function(data) {
                $('.alert-danger', "#home .container-fluid").fadeOut(function (){
                    $(this).remove();
                });
                $('.packs').html("");

                $.each(data, function (i, e){
                    var row = $('<tr data-id="'+e.id+'" data-size="'+e.size+'"><td>'+e.size+'</td><td class="text-center"><button class="edit btn btn-primary"><i class="fa fa-pencil-square-o"></i></button></td><td class="text-center"><button class="delete btn btn-danger"><i class="fa fa-trash"></i></button></td></tr>');
                    $('.packs').append(row);
                });

                config = data;

                value = parseInt($('input[name="order"]').val());
                refreshPacks();
            }
        });
    }

    function refreshPacks(){
        if (typeof value == 'undefined')
            return;

        if (isNaN(value))
            value = 0;
        
        packs = [];

        $.each(config, function (i, e){
            var amount = parseInt(value / e.size);

            if (amount >= 1)
            {
                packs.push({
                    size: e.size,
                    amount: amount
                });

                value -= amount * e.size;
            }
        });

        if (value > 0)
        {
            var closest = config.reduce(function(prev, curr) {
                return (Math.abs(curr.size - value) < Math.abs(prev.size - value) ? curr : prev);
            });

            packs.push({
                size: closest.size,
                amount: 1
            });
        }

        if (packs.length == 0)
            $('.pack-results').html('<tr><td class="text-center" colspan="2">No Packs required.</td></tr>');
        else 
        {
            $('.pack-results').html('');

            $.each(packs, function (i, e){
                $('.pack-results').append('<tr><td>'+e.size+'</td><td>'+e.amount+'</td></tr>');
            });
        }
    }

    $('input[name="order"]').on('input', function (){
        value = parseInt($(this).val());

        refreshPacks();                
    });

    $(document).on('click.edit', '.edit.btn', function (){
        var row = $(this).parents('tr');
        $('input[name="id"]','#edit-dialog').val(row.data('id'));
        $('input[name="size"]','#edit-dialog').val(row.data('size'));

        $('#edit-dialog').modal('show');
    });

    $(document).on('click.add', '.add.btn', function (){
        $('#add-dialog').modal('show');
    });

    $('button.btn.update').on('click', function (){
        var button = $(this);
        var dialog = button.parents('.modal');
        button.prop('disabled', true);
        var id = $('input[name="id"]', dialog).val();
        var size = $('input[name="size"]', dialog).val();
        var pack = {};

        if (parseInt(id) && !isNaN(id))
            pack['id'] = parseInt(id);

        if (parseInt(size) && !isNaN(size))
            pack['size'] = parseInt(size);
        else 
        {
            error("Invalid size entry.", "#"+dialog.attr('id') + " .modal-body");
            button.prop('disabled', false);
            return;
        }

        $.ajax({
            url: '/api/save',
            type: 'POST',
            data: {
                pack: pack
            },
            error: function() {
                error("Unable to save pack update.", "#"+dialog.attr('id') + " .modal-body");
                button.prop('disabled', false);
            },
            success: function(response) {
                if (response.success)
                {
                    refreshConfig();
                    button.prop('disabled', false);

                    $('input', dialog).val('');
                    dialog.modal('hide');
                }
            }
        });
    });

    $(document).on('click', 'button.btn.delete', function (){
        var button = $(this);
        var row = $(this).parents('tr');
        var id = row.data('id');

        $.ajax({
            url: '/api/delete?id='+id,
            type: 'DELETE',
            error: function() {
                error("Unable to delete pack.", "#config .container-fluid");
                button.prop('disabled', false);
            },
            success: function(response) {
                if (response.success)
                {
                    refreshConfig();
                    button.prop('disabled', false);
                }
            }
        });
    });

    refreshConfig();
    setInterval(function (){
        refreshConfig();
    }, 30000);
});