<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class BananasController extends Controller
{
    private $filePath = __DIR__. '/../../../public/data.json';

    public function index()
    {        
        return view('index');
    }

    /**
     * Method for retrieving the configuration information.
     * Configuration information includes all current pack sizes.
     * @param Illuminate\Http\Request $request Object containing all information about the request.
     * @return Illuminate\Http\Response Object containing information about the response that will be returned to the user. 
     */
    public function getPackConfig(Request $request) 
    {   
        $data = new Collection(json_decode(file_get_contents($this->filePath), true));

        $sort = $request->get('sort', 'size');
        $order = $request->get('order', 'desc');

        if (!in_array($sort, ['id', 'size']))
            return response()
                ->json([
                    'message' => 'Unable to sort data.'
                ], 400);

        if ($order == 'asc')
            $data = $data->sortBy($sort)->values();
        else 
            $data = $data->sortByDesc($sort)->values();

        return response()
            ->json($data);
    }

    public function deletePack(Request $request)
    {
        $id = $request->get('id', []);

        $data = new Collection(json_decode(file_get_contents($this->filePath), true));

        //Key by their ids.
        $data = $data->keyBy('id');

        if (empty($id) || empty($data[$id]))
            return response()
                ->json([
                    'success' => false
                ], 400);
        
        //Remove the pack.
        $data = $data->forget($id);

        file_put_contents($this->filePath, json_encode($data->values()));
        
        return response()
            ->json([
                'success' => true
            ]);
    }

    /**
     * Method for updating or saving a new pack.
     * @param Illuminate\Http\Request $request Object containing all information about the request.
     * @return Illuminate\Http\Response Object containing information about the response that will be returned to the user.
     */
    public function savePack(Request $request)
    {
        $pack = $request->get('pack', []);

        if (empty($pack))
            return response()
                ->json([
                    'success' => false
                ], 400);
        
        $data = new Collection(json_decode(file_get_contents($this->filePath), true));
        
        //Key by their ids.
        $data = $data->keyBy('id');
        
        if (!empty($pack['id']) && !empty($data[$pack['id']]))
        {
            $data[$pack['id']] = $pack;

            file_put_contents($this->filePath, json_encode($data->values()));

            return response()
                ->json([
                    'success' => true
                ], 200);
        }


        $newId = $data->max('id') + 1;
        $data->push([
            'id' => $newId,
            'size' => $pack['size']
        ]);

        file_put_contents($this->filePath, json_encode($data->values()));

        return response()
            ->json([
                'success' => true
            ]);
    }
}
