@extends('layout.master')

@section('content')
    
    <div class="tab-container col-md-10 offset-md-1 pt-2">
        <h2 class="app-title">Brucie’s Banana Pack Preparer <small>&copy; Brucie’s Banana Bazaar</small></h2>
        <ul class="nav nav-tabs" id="tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"><i class="fa fa-home"></i> Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="config-tab" data-toggle="tab" href="#config" role="tab" aria-controls="config" aria-selected="false"><i class="fa fa-cogs"></i> Config</a>
            </li>
        </ul>
        <div class="tab-content border-top-0" id="tabContent">
            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                <div class="container-fluid mt-1 pt-2 col-md-8 offset-md-2">
                    <h3>Order Query</h3>
                    <div class="form-group">
                        Enter your banana order to size:
                        <input class="form-control" placeholder="Enter banana order amount: e.g. 501" type="text" name="order" />
                        <small>Typing in the bananas ordered will automatically update the packs required.</small>
                    </div>
                    <hr />
                    <div>
                        Packs required for order;
                        <table class="table table-striped">
                            <thead>
                                <th>Pack Size</th>
                                <th>Amount</th>
                            </thead>
                            <tbody class="pack-results">
                                <tr><td class="text-center" colspan="2">Enter order size.</td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="config" role="tabpanel" aria-labelledby="config-tab">
                <div class="container-fluid mt-1 pt-2 col-md-10 offset-md-1">
                    <h3>Pack Configuration</h3>
                    <p>Change the pack configuration below;<button class="pull-right mb-1 add btn btn-primary">
                        <i class="fa fa-plus"></i> Add Pack
                    </button></p>
                    
                    <table class="table table-striped">
                        <thead>
                            <th>Pack Size</th>
                            <th class="button">Edit</th>
                            <th class="button">Delete</th>
                        </thead>
                        <tbody class="packs">
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @include('partials.edit-dialog')
    @include('partials.add-dialog')
    
    <script src="/js/main.js"></script>
@endsection