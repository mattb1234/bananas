<div class="modal fade" id="edit-dialog" tabindex="-1" role="dialog" aria-labelledby="editDialogLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editDialogLabel">Edit Pack</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" name="id" value="" />
                
                <div class="form-group">
                    Pack Size:
                    <input class="form-control" type="text" name="size" />
                    <small>Once a pack has been edited required packs for an active pack request will automatically be recalculated.</small>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                
                <button class="float-right update btn btn-primary">
                    <i class="fa fa-save"></i> Update
                </button>
            </div>
        </div>
    </div>
</div>