<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', ['uses' => 'BananasController@index']);

$router->get('/api/config', ['uses' => 'BananasController@getPackConfig', 'as' => 'config']);
$router->post('/api/save', ['uses' => 'BananasController@savePack', 'as' => 'save']);
$router->delete('/api/delete', ['uses' => 'BananasController@deletePack', 'as' => 'delete']);
